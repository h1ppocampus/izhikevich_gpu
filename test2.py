import numpy as np
import izh_numpy
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use('Qt5Agg')
izh = izh_numpy.Izhikevich(store_v=True, dt=0.1, sub_dt=0.1, with_delays=True)
t0 = time.time()
spike_num = 0
v_store = []
i_store = []
u_store = []
for i in range(1):
    izh.step()
#    print('Step {}'.format(i))
#    print(izh.spiked)
    spike_num += izh.spiked.shape[0]
    print(i)
    a = np.concatenate(izh.v_store, axis=1)
    plt.plot(a.T)
t1 = time.time()

print(t1-t0)
