import torch
import numpy as np
import pandas as pd

anatomy_tab = pd.DataFrame({'Structure': ['Cortex', 'Cortex'],
                            'Neurotransmitter': ['Glutamate', 'GABA'],
                            'Number of neurons': [8000, 2000],
                            'a_scale': [0.02, 0.08],
                            'a_bias': [0, 0.02],
                            'b_scale': [0, -0.05],
                            'b_bias': [0.2, 0.25],
                            'c_scale': [15, 0],
                            'c_bias': [-65, -65],
                            'd_scale': [-6, 0],
                            'd_bias': [8, 2],
                            'ge_mean_noise': [0.123, 0.123],
                            'gi_mean_noise': [0.322, 0.322],
                            'noise_te': [2.7, 2.7],
                            'noise_ti': [10.5, 10.5],
                            'noise_sigma_e': [0.0163, 0.0163],
                            'noise_sigma_i': [0.0265, 0.0265],
                            'noise_V_e': [0, 0],
                            'noise_V_i': [-80, 80]
                            })

neurotransmitters_tab = pd.DataFrame({'Neurotransmitter': ['Glutamate', 'GABA'],
                                      'Time constant g': [1, 2],
                                      'V': [0, -80]})

S_dist = np.array((['log_norm', 'log_norm'], ['norm_nonneg', 'norm_nonneg']))

S_bias = np.array(([-5.543, -5.543], [0.0217, 0.0217]))
S_scale = np.array(([1.3, 1.3], [0.00171, 0.00171]))
S_prob = np.array(([0.5, 0.5], [0.5, 0.5]))
delay_start = np.array(([3, 3], [2, 2]))  # ms
delay_end = np.array(([5, 5], [4, 4]))  # ms


def replace_neg_conductance(non_0_synapse, scale, bias):

    replace = non_0_synapse < 0
    non_0_synapse[replace] = torch.randn(torch.count_nonzero(replace)) * scale + bias
    if any(non_0_synapse < 0):
        replace_neg_conductance(non_0_synapse, scale, bias)

    return non_0_synapse


class AbcdParams:

    def __init__(self, anatomy):

        a_list = []
        b_list = []
        c_list = []
        d_list = []

        for i, n_neurons in enumerate(anatomy['Number of neurons']):

            uniform_rand_array = torch.rand(n_neurons)

            a_list.append(anatomy['a_scale'][i] * uniform_rand_array + anatomy['a_bias'][i])
            b_list.append(anatomy['b_scale'][i] * uniform_rand_array + anatomy['b_bias'][i])
            c_list.append(anatomy['c_scale'][i] * uniform_rand_array + anatomy['c_bias'][i])
            d_list.append(anatomy['d_scale'][i] * uniform_rand_array + anatomy['d_bias'][i])

        self.a = torch.concat(a_list)
        self.b = torch.concat(b_list)
        self.c = torch.concat(c_list)
        self.d = torch.concat(d_list)


class Synapse:

    def __init__(self, anatomy=anatomy_tab, neurotransmitters=neurotransmitters_tab, S_dist=S_dist, S_bias=S_bias,
                 S_scale=S_scale, S_prob=S_prob, delay_start=delay_start, delay_end=delay_end, with_delay=False, dt=1):

        self.anatomy = anatomy
        self.neurotransmitters = neurotransmitters

        self.nt_V = torch.from_numpy(self.neurotransmitters['V'].values)

        self.g = torch.zeros(anatomy['Number of neurons'].sum(), len(neurotransmitters))

        self.S_bias = S_bias
        self.S_scale = S_scale

        self.S_dist = S_dist

        self.start_delay = delay_start
        self.end_delay = delay_end

        self.connection_prob = S_prob

        self.S = None
        self.S_delay = None

        self.with_delay = with_delay
        self.delay_range = np.max(self.end_delay) - np.max(self.start_delay)

        self.g_delay = torch.zeros((anatomy['Number of neurons'].sum(), self.delay_range, len(neurotransmitters)))

        self.dt = dt

        self.I_synapse = None

        self.cumulative_n_neurons = torch.zeros(len(neurotransmitters))
        for i, nt in enumerate(neurotransmitters['Neurotransmitter']):

            if i == 0:
                self.cumulative_n_neurons[i] = \
                    torch.from_numpy(anatomy['Number of neurons'][anatomy['Neurotransmitter'] == nt].values)
            else:
                self.cumulative_n_neurons[i] = \
                    torch.from_numpy(anatomy['Number of neurons'][anatomy['Neurotransmitter'] == nt].values) + \
                    self.cumulative_n_neurons[i-1]

        self.cumulative_n_neurons = self.cumulative_n_neurons.int()

        self.populate_S()

    def populate_S(self):

        self.connected = torch.zeros((self.anatomy['Number of neurons'].sum(), self.anatomy['Number of neurons'].sum()))

        if self.with_delay:
            least_delay = np.min(self.start_delay)
            max_delay = np.max(self.end_delay)
            delay_index = int(np.round((max_delay-least_delay)/self.dt))
            self.S_delay = torch.zeros((self.anatomy['Number of neurons'].sum(), self.anatomy['Number of neurons'].sum()
                                        , delay_index + 1))
            start_index = np.round(self.start_delay - least_delay)/self.dt
            end_index = np.round(self.end_delay - least_delay)/self.dt
        else:
            self.S = torch.zeros((self.anatomy['Number of neurons'].sum(), self.anatomy['Number of neurons'].sum()))

        start_row = 0

        for i, n_afferent_neurons in enumerate(self.anatomy['Number of neurons']):

            start_col = 0

            for j, n_efferent_neurons in enumerate(self.anatomy['Number of neurons']):

                sub_connected = (torch.rand((n_afferent_neurons,
                                             n_efferent_neurons)) > self.connection_prob[i, j]).bool()

                if self.S_dist[i][j] == 'uni':
                    non_0_synapse = self.S_scale[i][j] * torch.rand(torch.count_nonzero(sub_connected))\
                                    + self.S_bias[i][j]
                elif self.S_dist[i][j] == 'norm':
                    non_0_synapse = self.S_scale[i][j] * torch.randn(torch.count_nonzero(sub_connected)) \
                                    + self.S_bias[i][j]
                elif self.S_dist[i][j] == 'norm_nonneg':
                    non_0_synapse = self.S_scale[i][j] * torch.randn(torch.count_nonzero(sub_connected)) \
                                    + self.S_bias[i][j]
                    replace_neg_conductance(non_0_synapse, self.S_scale[i][j], self.S_bias[i][j])
                elif self.S_dist[i][j] == 'log_norm':
                    non_0_synapse = torch.zeros(torch.count_nonzero(sub_connected)).log_normal_(self.S_bias[i][j],
                                                                                                self.S_scale[i][j])
                else:
                    raise ValueError('{} is not a supported distribution'.format(self.S_dist[i][j]))

                if self.with_delay:
                    sub_S_delay = torch.zeros((n_afferent_neurons, n_efferent_neurons, delay_index + 1))
                    sub_S_delay[sub_connected, torch.randint(low=int(start_index[i, j]), high=int(end_index[i][j]) + 1,
                                                             size=[non_0_synapse.size()[0]])] \
                        = non_0_synapse
                    self.S_delay[start_row:(start_row + n_afferent_neurons), start_col:(start_col + n_efferent_neurons),
                    :] = sub_S_delay
                else:
                    sub_S = torch.zeros((n_afferent_neurons,
                                         n_efferent_neurons))
                    sub_S[sub_connected] = non_0_synapse
                    self.S[start_row:(start_row + n_afferent_neurons), start_col:(start_col + n_efferent_neurons)] = \
                        sub_S

                start_col += n_efferent_neurons

            start_row += n_afferent_neurons


class BackgroundNoise:

    def __init__(self, dt, device, n_timesteps, anatomy_table, batch_size=1):

        self.dt = dt
        self.n_timesteps = n_timesteps
        self.anatomy_table = anatomy_table.copy()
        self.batch_size = batch_size
        self.batch_position = 0

        self.device = device

        self.noise_e = None
        self.noise_i = None

        n_anatomy = len(self.anatomy_table)
        cumulative_n_neurons = np.zeros(len(anatomy_table))
        for i in range(len(self.anatomy_table)):
            if i == 0:
                cumulative_n_neurons[i] = self.anatomy_table['Number of neurons'].iloc[i]
            else:
                cumulative_n_neurons[i] = cumulative_n_neurons[i-1] \
                                       + self.anatomy_table['Number of neurons'].iloc[i]

        self.anatomy_table['cumulative n neurons'] = cumulative_n_neurons

        self.n_neurons = int(self.anatomy_table['cumulative n neurons'].iloc[-1])
        self.ge_mean_noise = np.zeros(int(self.n_neurons))
        self.gi_mean_noise = np.zeros(int(self.n_neurons))
        self.noise_te = np.zeros(int(self.n_neurons))
        self.noise_ti = np.zeros(int(self.n_neurons))
        self.noise_sigma_e = np.zeros(int(self.n_neurons))
        self.noise_sigma_i = np.zeros(int(self.n_neurons))
        self.noise_V_e = np.zeros(int(self.n_neurons))
        self.noise_V_i = np.zeros(int(self.n_neurons))

        for i, cumul_n_neurons in enumerate(self.anatomy_table['cumulative n neurons']):

            neuron_index_end = int(self.anatomy_table['cumulative n neurons'].iloc[i])

            if i == 0:

                self.ge_mean_noise[0:neuron_index_end] = self.anatomy_table['ge_mean_noise'].iloc[i]
                self.gi_mean_noise[0:neuron_index_end] = self.anatomy_table['gi_mean_noise'].iloc[i]
                self.noise_te[0:neuron_index_end] = self.anatomy_table['noise_te'].iloc[i]
                self.noise_ti[0:neuron_index_end] = self.anatomy_table['noise_ti'].iloc[i]
                self.noise_sigma_e[0:neuron_index_end] = self.anatomy_table['noise_sigma_e'].iloc[i]
                self.noise_sigma_i[0:neuron_index_end] = self.anatomy_table['noise_sigma_i'].iloc[i]
                self.noise_V_e[0:neuron_index_end] = self.anatomy_table['noise_V_e'].iloc[i]
                self.noise_V_i[0:neuron_index_end] = self.anatomy_table['noise_V_i'].iloc[i]

            else:

                neuron_index_start = int(self.anatomy_table['cumulative n neurons'].iloc[i-1])

                self.ge_mean_noise[neuron_index_start:neuron_index_end] = self.anatomy_table['ge_mean_noise'].iloc[i]
                self.gi_mean_noise[neuron_index_start:neuron_index_end] = self.anatomy_table['gi_mean_noise'].iloc[i]
                self.noise_te[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_te'].iloc[i]
                self.noise_ti[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_ti'].iloc[i]
                self.noise_sigma_e[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_sigma_e'].iloc[i]
                self.noise_sigma_i[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_sigma_i'].iloc[i]
                self.noise_V_e[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_V_e'].iloc[i]
                self.noise_V_i[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_V_i'].iloc[i]

        self.ge_noise = self.ge_mean_noise
        self.gi_noise = self.gi_mean_noise

        self.noise_e_coef = ((2*self.noise_sigma_e**2)/self.noise_te) ** (1/2)
        self.noise_i_coef = ((2*self.noise_sigma_i**2)/self.noise_ti) ** (1/2)

        self.generate_noise_conductance()

        self.ge_noise = torch.from_numpy(self.ge_noise)
        self.gi_noise = torch.from_numpy(self.gi_noise)
        self.ge_mean_noise = torch.from_numpy(self.ge_mean_noise)
        self.gi_mean_noise = torch.from_numpy(self.gi_mean_noise)
        self.noise_te = torch.from_numpy(self.noise_te)
        self.noise_ti = torch.from_numpy(self.noise_ti)

    def generate_noise_conductance(self):

        self.noise_i = torch.randn(self.batch_size, self.n_neurons, device=self.device) * self.noise_i_coef
        self.noise_e = torch.randn(self.batch_size, self.n_neurons, device=self.device) * self.noise_e_coef
        self.batch_position = 0

    def update_g_noise(self):

        self.ge_noise = ((self.ge_noise - self.ge_mean_noise)/self.noise_te + self.noise_e[self.batch_position, :]) * \
                        self.dt
        self.gi_noise = ((self.gi_noise - self.gi_mean_noise)/self.noise_ti + self.noise_i[self.batch_position, :]) * \
                        self.dt

        self.batch_position += 1

        if self.batch_position == self.batch_size:
            self.generate_noise_conductance()


class Izhikevich:

    def __init__(self, unlimited_steps=False, n_timesteps=10 ** (5), dt=1, sub_dt=0.1,
                 I_noise_excit_scale=5, I_noise_inhib_scale=2, with_delays=False,
                 anatomy=anatomy_tab, neurotransmitters=neurotransmitters_tab, S_dist=S_dist, S_bias=S_bias,
                 S_scale=S_scale, S_prob=S_prob, delay_start=delay_start, delay_end=delay_end, device='cpu',
                 random_batch_size=1, store_spikes=True, store_v=False, store_u=False):

        self.device = torch.device(device)

        self.n_neurons = anatomy['Number of neurons'].sum()
        self.unlimited_steps = unlimited_steps
        self.n_timesteps = n_timesteps
        self.dt = dt
        self.sub_dt = sub_dt
        self.n_steps = 0

        self.with_delay = with_delays

        self.anatomy = anatomy.sort_values(by='Neurotransmitter')
        self.neurotransmitter = neurotransmitters.sort_values(by='Neurotransmitter')
        self.cumulative_n_neurons_anatomy = np.cumsum(self.anatomy['Number of neurons'].values)

        self.I_noise_excit_scale = I_noise_excit_scale
        self.I_noise_inhib_scale = I_noise_inhib_scale

        self.abcdParams = AbcdParams(anatomy)

        self.synapse = Synapse(anatomy=anatomy, neurotransmitters=neurotransmitters, S_dist=S_dist, S_bias=S_bias,
                               S_scale=S_scale, S_prob=S_prob, delay_start=delay_start, delay_end=delay_end,
                               with_delay=False, dt=1)
        self.synapse.populate_S()

        self.v = -65 * torch.ones(self.n_neurons).to(self.device)
        self.u = (self.abcdParams.b * self.v).to(self.device)

        self.threshold = torch.ones(self.n_neurons) * 30
        self.spiked = None

        self.store_spikes = store_spikes
        if store_spikes:
            self.spike_store = []

        self.store_v = store_v
        if store_v:
            self.v_store = []

        self.store_u = store_u
        if store_u:
            self.u_store = []

        self.random_batch_size = random_batch_size
        self.background_noise = BackgroundNoise(dt, device, n_timesteps, self.anatomy, batch_size=random_batch_size)

        self.i_e_noise = None
        self.i_i_noise = None

        self.i = None

        self.has_resting_membrane_potenital = None

    def stable_resting_membrane_potential(self):
        self.has_resting_membrane_potenital = ((5 - self.abcdParams.b) ** 2 - 4 * self.abcdParams.a * 140) >= 0

    def epsp_dist(self):
        for i, n_neurons in enumerate(self.synapse.cumulative_n_neurons):
            if i == 0:
                s_by_anatomy = self.synapse.S[0:self.cumulative_n_neurons_anatomy[0], :].reshape(-1)
                g_check = s_by_anatomy[~(s_by_anatomy == 0)]
                b = 0.2
                v = - (5 - b) - ((5 - b)**2 - 4*0.04*140)**(1/2)


    def update_g(self):

        #needs to for specific neurotransmitters

        for i, t_g in enumerate(self.synapse.neurotransmitters['Time constant g']):

            if i == 0:
                self.synapse.g[:, i] = (- self.synapse.g[:, i] / t_g \
                + torch.sum(self.synapse.S[0:self.synapse.cumulative_n_neurons[i], :] \
                                [self.spiked[(0 <= self.spiked) & (self.spiked <self.synapse.cumulative_n_neurons[i])]
                            , :], dim=0)) * self.dt
            else:
                self.synapse.g[:, i] = (- self.synapse.g[:, i] / t_g \
                + torch.sum(self.synapse.S[self.synapse.cumulative_n_neurons[i-1]:
                                           self.synapse.cumulative_n_neurons[i], :] \
                            [self.spiked[(self.synapse.cumulative_n_neurons[i-1] <= self.spiked) &
                                         (self.spiked < self.synapse.cumulative_n_neurons[i])]
                             - self.synapse.cumulative_n_neurons[i-1], :], dim=0)) * self.dt

    def update_g_delay(self):

        for i, t_g in enumerate(self.synapse.neurotransmitters['Time constant g']):

            self.synapse.g[:, i] += self.synapse.g[:, i] / t_g + self.synapse.g_delay[:, 0, i]
            self.synapse.g_delay[:, :-1, i] = self.synapse.g_delay[:, 1:, i]
            self.synapse.g_delay[:, -1, i] = torch.zeros(self.n_neurons)

            if i == 0:
                self.synapse.g_delay[:, -self.synapse.g_delay.shape[2]:] \
                    += torch.sum(self.synapse.g_delay[self.synapse.cumulative_n_neurons[i-1]:
                                                      self.synapse.cumulative_n_neurons[i], :, :] \
                                     [self.spiked[0:self.synapse.cumulative_n_neurons[i]], :, :], dim=0)
            else:
                self.synapse.g_delay[:, -self.synapse.g_delay.shape[2]:] \
                    += torch.sum(self.synapse.g_delay[self.synapse.cumulative_n_neurons[i-1]:
                                                      self.synapse.cumulative_n_neurons[i]] \
                                     [self.spiked[self.synapse.cumulative_n_neurons[i-1]:
                                      self.synapse.cumulative_n_neurons[i]], :, :], dim=0)

    def update_I_synapes(self):

        for i, V in enumerate(self.synapse.nt_V):

            if i == 0:
                self.synapse.I_synapse = self.synapse.g[:, i] * (self.v - V)
            else:
                self.synapse.I_synapse += self.synapse.g[:, i] * (self.v - V)

    def update_i_noise(self):

        self.i_e_noise = self.background_noise.ge_noise * (self.v - self.background_noise.noise_V_e)
        self.i_i_noise = self.background_noise.gi_noise * (self.v - self.background_noise.noise_V_i)

    def sum_current(self):

        self.i = self.i_i_noise + self.i_e_noise + self.synapse.I_synapse

    def update_v(self):
        self.v += self.sub_dt * (0.04 * self.v ** 2 + 5 * self.v + 140 - self.u + self.i)

    def step(self):

        print(self.v)
        if self.store_spikes:
            self.spike_store.append(self.spiked)
        if self.store_v:
            self.v_store.append(self.v)
        if self.store_u:
            self.u_store.append(self.u)

        self.update_I_synapes()

        if self.with_delay:
            self.update_g_delay()
        else:
            self.update_g()

        self.background_noise.update_g_noise()

        self.update_i_noise()
        print(self.i_e_noise)

        self.sum_current()

        self.v[self.spiked] = self.abcdParams.c[self.spiked]
        self.u[self.spiked] += self.abcdParams.d[self.spiked]

        print(self.v)
        print(self.i)

        for _ in range(int(self.dt / self.sub_dt)):
            self.update_v()
            self.u += self.abcdParams.a * (self.v * self.abcdParams.b - self.u) * self.sub_dt

        self.n_steps += 1

        self.spiked = torch.where(self.v > 30)[0]
