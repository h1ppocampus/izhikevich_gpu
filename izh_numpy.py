import torch
import numpy as np
import pandas as pd


def stable_resting_membrane_potential(b):
    return ((5 - b) ** 2 - 4 * 0.04 * 140) < 0


def spiking_threshold(b):
    return ((b - 5) + ((5 - b) ** 2 - 4 * 0.04 * 140) ** (1 / 2)) / (2 * 0.04)


def resting_membrane_pot(b):
    return ((b - 5) - ((5 - b) ** 2 - 4 * 0.04 * 140) ** (1 / 2)) / (2 * 0.04)


def epsp_mag(a, b, synaptic_weight, t_g, v_g, dt, time_span):
    resting_mem = resting_membrane_pot(b)
    v = resting_mem
    u = b * v
    max_v = v
    spike_thresh = spiking_threshold(b)
    g = 0
    for i in range(int(time_span / dt)):
        g += (-g / t_g + synaptic_weight) * dt
        I = g * (v - v_g)
        v += (0.04 * v ** 2 + 5 * v + 140 - I - u) * dt
        print(v)
        u += a * (b * v - u)
        if v > max_v:
            max_v = v
            if v > spike_thresh:
                break
    return max_v - resting_mem


def epsp_mag_array(a, b, synaptic_weight, t_g, v_g, dt, time_span):
    resting_mem = resting_membrane_pot(b)
    v = np.repeat(resting_mem, synaptic_weight.shape)
    u = b * v
    max_v = v.copy()
    spike_thresh = spiking_threshold(b)
    max_possible_epsp = spike_thresh - resting_mem
    g = 0
    for i in range(int(time_span / dt)):
        g += (-g / t_g + synaptic_weight) * dt
        I = g * (v - v_g)
        v += (0.04 * v ** 2 + 5 * v + 140 - I - u) * dt
        print(v)
        u += a * (b * v - u)
        max_v_logic = v > max_v
        if any(max_v_logic):
            max_v[max_v_logic] = v[max_v_logic]
    max_v[max_v > max_possible_epsp] = max_possible_epsp
    return max_v - resting_mem


'''
anatomy_tab = pd.DataFrame({'Structure': ['Cortex', 'Cortex'],
                            'Neurotransmitter': ['Glutamate', 'GABA'],
                            'Number of neurons': [800, 200],
                            'a_scale': [0.02, 0.08],
                            'a_bias': [0, 0.02],
                            'b_scale': [0, -0.05],
                            'b_bias': [0.2, 0.25],
                            'c_scale': [15, 0],
                            'c_bias': [-65, -65],
                            'd_scale': [-6, 0],
                            'd_bias': [8, 2],
                            'ge_mean_noise': [0.123, 0.123],
                            'gi_mean_noise': [0.322, 0.322],
                            'noise_te': [2.7, 2.7],
                            'noise_ti': [10.5, 10.5],
                            'noise_sigma_e': [0.0163, 0.0163],
                            'noise_sigma_i': [0.0265, 0.0265],
                            'noise_V_e': [0, 0],
                            'noise_V_i': [-80, -80]
                            })
'''

noise_scale = 9

anatomy_tab = pd.DataFrame({'Structure': ['Cortex', 'Cortex'],
                            'Neurotransmitter': ['Glutamate', 'GABA'],
                            'Number of neurons': [800, 200],
                            'a_scale': [0, 0],
                            'a_bias': [0.02, 0.02],
                            'b_scale': [0, 0],
                            'b_bias': [0.2, 0.2],
                            'c_scale': [0, 0],
                            'c_bias': [-65, -65],
                            'd_scale': [0, 0],
                            'd_bias': [8, 2],
                            'ge_mean_noise': [0.123 * noise_scale, 0.123 * noise_scale],
                            'gi_mean_noise': [0.322 * noise_scale, 0.322 * noise_scale],
                            'noise_te': [2.7, 2.7],
                            'noise_ti': [10.5, 10.5],
                            'noise_sigma_e': [0.0163 * noise_scale, 0.0163 * noise_scale],
                            'noise_sigma_i': [0.0265 * noise_scale, 0.0265 * noise_scale],
                            'noise_V_e': [0, 0],
                            'noise_V_i': [-80, -80]
                            })

neurotransmitters_tab = pd.DataFrame({'Neurotransmitter': ['Glutamate', 'GABA'],
                                      'Time constant g': [1, 2],
                                      'V': [0, -80]})

S_dist = np.array((['log_norm', 'log_norm'], ['norm_nonneg', 'norm_nonneg']))

S_bias = np.array(([-5.38, -5.38], [0.0228, 0.0228]))
S_scale = np.array(([0.833, 0.833], [0.00180, 0.00180]))
S_prob = np.array(([0.125, 0.125], [0.25, 0.25]))
delay_start = np.array(([3, 3], [2, 2]))  # ms
delay_end = np.array(([5, 5], [4, 4]))  # ms


def replace_neg_conductance(non_0_synapse, scale, bias):
    replace = non_0_synapse < 0
    non_0_synapse[replace] = np.random.randn(np.count_nonzero(replace)) * scale + bias
    if any(non_0_synapse < 0):
        replace_neg_conductance(non_0_synapse, scale, bias)

    return non_0_synapse


class AbcdParams:

    def __init__(self, anatomy):
        a_list = []
        b_list = []
        c_list = []
        d_list = []

        for i, n_neurons in enumerate(anatomy['Number of neurons']):
            uniform_rand_array = np.random.rand(n_neurons)

            a_list.append(anatomy['a_scale'][i] * uniform_rand_array + anatomy['a_bias'][i])
            b_list.append(anatomy['b_scale'][i] * uniform_rand_array + anatomy['b_bias'][i])
            c_list.append(anatomy['c_scale'][i] * uniform_rand_array + anatomy['c_bias'][i])
            d_list.append(anatomy['d_scale'][i] * uniform_rand_array + anatomy['d_bias'][i])

        self.a = np.concatenate(a_list)
        self.b = np.concatenate(b_list)
        self.c = np.concatenate(c_list)
        self.d = np.concatenate(d_list)


class Synapse:

    def __init__(self, anatomy=anatomy_tab, neurotransmitters=neurotransmitters_tab, S_dist=S_dist, S_bias=S_bias,
                 S_scale=S_scale, S_prob=S_prob, delay_start=delay_start, delay_end=delay_end, with_delay=False, dt=1):

        self.anatomy = anatomy
        self.neurotransmitters = neurotransmitters

        self.nt_V = self.neurotransmitters['V'].values

        self.g = np.zeros((anatomy['Number of neurons'].sum(), len(neurotransmitters)))

        self.S_bias = S_bias
        self.S_scale = S_scale

        self.S_dist = S_dist

        self.start_delay = delay_start
        self.end_delay = delay_end

        self.connection_prob = S_prob

        self.S = None
        self.S_delay = None

        self.dt = dt

        self.with_delay = with_delay
        self.max_delay = np.max(self.end_delay)
        self.max_delay_index = int(self.max_delay / self.dt)
        self.min_delay = np.min(self.start_delay)
        self.min_delay_index = int(self.min_delay / self.dt)
        self.delay_range = self.max_delay_index - self.min_delay_index

        self.g_delay = np.zeros((anatomy['Number of neurons'].sum(), self.max_delay_index, len(neurotransmitters)))

        self.I_synapse = None

        self.cumulative_n_neurons = np.zeros(len(neurotransmitters))
        for i, nt in enumerate(neurotransmitters['Neurotransmitter']):

            if i == 0:
                self.cumulative_n_neurons[i] = \
                    anatomy['Number of neurons'][anatomy['Neurotransmitter'] == nt].values
            else:
                self.cumulative_n_neurons[i] = \
                    anatomy['Number of neurons'][anatomy['Neurotransmitter'] == nt].values + \
                    self.cumulative_n_neurons[i - 1]

        self.cumulative_n_neurons = self.cumulative_n_neurons.astype('int')

        self.populate_S()
        self.connected = None

    def populate_S(self):

        self.connected = np.zeros((self.anatomy['Number of neurons'].sum(), self.anatomy['Number of neurons'].sum()))

        if self.with_delay:
            least_delay = np.min(self.start_delay)
            max_delay = np.max(self.end_delay)
            delay_index = int(np.round((max_delay - least_delay) / self.dt))
            self.S_delay = np.zeros((self.anatomy['Number of neurons'].sum(), self.anatomy['Number of neurons'].sum()
                                     , self.delay_range + 1))
            start_index = np.round(self.start_delay - least_delay) / self.dt
            end_index = np.round(self.end_delay - least_delay) / self.dt
        else:
            self.S = np.zeros((self.anatomy['Number of neurons'].sum(), self.anatomy['Number of neurons'].sum()))

        start_row = 0

        for i, n_afferent_neurons in enumerate(self.anatomy['Number of neurons']):

            start_col = 0

            for j, n_efferent_neurons in enumerate(self.anatomy['Number of neurons']):

                sub_connected = (np.random.rand(n_afferent_neurons,
                                                n_efferent_neurons) < self.connection_prob[i, j]).astype('bool')

                if self.S_dist[i][j] == 'uni':
                    non_0_synapse = self.S_scale[i][j] * np.random.rand(np.count_nonzero(sub_connected)) \
                                    + self.S_bias[i][j]
                elif self.S_dist[i][j] == 'norm':
                    non_0_synapse = self.S_scale[i][j] * np.radom.randn(np.count_nonzero(sub_connected)) \
                                    + self.S_bias[i][j]
                elif self.S_dist[i][j] == 'norm_nonneg':
                    non_0_synapse = self.S_scale[i][j] * np.random.randn(np.count_nonzero(sub_connected)) \
                                    + self.S_bias[i][j]
                    replace_neg_conductance(non_0_synapse, self.S_scale[i][j], self.S_bias[i][j])
                elif self.S_dist[i][j] == 'log_norm':
                    non_0_synapse = np.random.lognormal(size=np.count_nonzero(sub_connected), mean=self.S_bias[i][j],
                                                        sigma=self.S_scale[i][j])
                else:
                    raise ValueError('{} is not a supported distribution'.format(self.S_dist[i][j]))

                if self.with_delay:
                    sub_S_delay = np.zeros((n_afferent_neurons, n_efferent_neurons, self.delay_range + 1))
                    sub_S_delay[sub_connected, np.random.randint(low=int(start_index[i, j]),
                                                                 high=int(end_index[i][j]) + 1,
                                                                 size=[non_0_synapse.size])] \
                        = non_0_synapse
                    self.S_delay[start_row:(start_row + n_afferent_neurons), start_col:(start_col + n_efferent_neurons),
                    :] = sub_S_delay
                else:
                    sub_S = np.zeros((n_afferent_neurons,
                                      n_efferent_neurons))
                    sub_S[sub_connected] = non_0_synapse
                    self.S[start_row:(start_row + n_afferent_neurons), start_col:(start_col + n_efferent_neurons)] = \
                        sub_S

                start_col += n_efferent_neurons

            start_row += n_afferent_neurons


class BackgroundNoise:

    def __init__(self, dt, device, n_timesteps, anatomy_table, batch_size=1):

        self.dt = dt
        self.n_timesteps = n_timesteps
        self.anatomy_table = anatomy_table.copy()
        self.batch_size = batch_size
        self.batch_position = 0

        self.device = device

        self.noise_e = None
        self.noise_i = None

        n_anatomy = len(self.anatomy_table)
        cumulative_n_neurons = np.zeros(len(anatomy_table))
        for i in range(len(self.anatomy_table)):
            if i == 0:
                cumulative_n_neurons[i] = self.anatomy_table['Number of neurons'].iloc[i]
            else:
                cumulative_n_neurons[i] = cumulative_n_neurons[i - 1] \
                                          + self.anatomy_table['Number of neurons'].iloc[i]

        self.anatomy_table['cumulative n neurons'] = cumulative_n_neurons

        self.n_neurons = int(self.anatomy_table['cumulative n neurons'].iloc[-1])
        self.ge_mean_noise = np.zeros(int(self.n_neurons))
        self.gi_mean_noise = np.zeros(int(self.n_neurons))
        self.noise_te = np.zeros(int(self.n_neurons))
        self.noise_ti = np.zeros(int(self.n_neurons))
        self.noise_sigma_e = np.zeros(int(self.n_neurons))
        self.noise_sigma_i = np.zeros(int(self.n_neurons))
        self.noise_V_e = np.zeros(int(self.n_neurons))
        self.noise_V_i = np.zeros(int(self.n_neurons))

        for i, cumul_n_neurons in enumerate(self.anatomy_table['cumulative n neurons']):

            neuron_index_end = int(self.anatomy_table['cumulative n neurons'].iloc[i])

            if i == 0:

                self.ge_mean_noise[0:neuron_index_end] = self.anatomy_table['ge_mean_noise'].iloc[i]
                self.gi_mean_noise[0:neuron_index_end] = self.anatomy_table['gi_mean_noise'].iloc[i]
                self.noise_te[0:neuron_index_end] = self.anatomy_table['noise_te'].iloc[i]
                self.noise_ti[0:neuron_index_end] = self.anatomy_table['noise_ti'].iloc[i]
                self.noise_sigma_e[0:neuron_index_end] = self.anatomy_table['noise_sigma_e'].iloc[i]
                self.noise_sigma_i[0:neuron_index_end] = self.anatomy_table['noise_sigma_i'].iloc[i]
                self.noise_V_e[0:neuron_index_end] = self.anatomy_table['noise_V_e'].iloc[i]
                self.noise_V_i[0:neuron_index_end] = self.anatomy_table['noise_V_i'].iloc[i]

            else:

                neuron_index_start = int(self.anatomy_table['cumulative n neurons'].iloc[i - 1])

                self.ge_mean_noise[neuron_index_start:neuron_index_end] = self.anatomy_table['ge_mean_noise'].iloc[i]
                self.gi_mean_noise[neuron_index_start:neuron_index_end] = self.anatomy_table['gi_mean_noise'].iloc[i]
                self.noise_te[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_te'].iloc[i]
                self.noise_ti[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_ti'].iloc[i]
                self.noise_sigma_e[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_sigma_e'].iloc[i]
                self.noise_sigma_i[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_sigma_i'].iloc[i]
                self.noise_V_e[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_V_e'].iloc[i]
                self.noise_V_i[neuron_index_start:neuron_index_end] = self.anatomy_table['noise_V_i'].iloc[i]

        self.ge_noise = self.ge_mean_noise
        self.gi_noise = self.gi_mean_noise

        self.noise_e_coef = ((2 * self.noise_sigma_e ** 2) / self.noise_te) ** (1 / 2)
        self.noise_i_coef = ((2 * self.noise_sigma_i ** 2) / self.noise_ti) ** (1 / 2)

        self.generate_noise_conductance()

    def generate_noise_conductance(self):

        self.noise_i = np.random.randn(self.batch_size, self.n_neurons) * self.noise_i_coef
        self.noise_e = np.random.randn(self.batch_size, self.n_neurons) * self.noise_e_coef
        self.batch_position = 0

    def update_g_noise(self):

        self.ge_noise = (-(self.ge_noise - self.ge_mean_noise) / self.noise_te + self.noise_e[self.batch_position, :]) * \
                        self.dt
        self.gi_noise = (-(self.gi_noise - self.gi_mean_noise) / self.noise_ti + self.noise_i[self.batch_position, :]) * \
                        self.dt

        self.batch_position += 1

        if self.batch_position == self.batch_size:
            self.generate_noise_conductance()


class Izhikevich:

    def __init__(self, unlimited_steps=False, n_timesteps=10 ** (5), dt=1, sub_dt=0.1,
                 I_noise_excit_scale=5, I_noise_inhib_scale=2, with_delays=False,
                 anatomy=anatomy_tab, neurotransmitters=neurotransmitters_tab, S_dist=S_dist, S_bias=S_bias,
                 S_scale=S_scale, S_prob=S_prob, delay_start=delay_start, delay_end=delay_end, device='cpu',
                 random_batch_size=1, store_spikes=True, store_v=False, store_u=False, preallocate_mem=True):

        self.n_neurons = anatomy['Number of neurons'].sum()
        self.unlimited_steps = unlimited_steps
        self.n_timesteps = n_timesteps
        self.dt = dt
        self.sub_dt = sub_dt
        self.n_steps = 0

        self.with_delay = with_delays

        self.anatomy = anatomy.sort_values(by='Neurotransmitter')
        self.neurotransmitter = neurotransmitters.sort_values(by='Neurotransmitter')
        self.cumulative_n_neurons_anatomy = np.cumsum(self.anatomy['Number of neurons'].values)

        self.I_noise_excit_scale = I_noise_excit_scale
        self.I_noise_inhib_scale = I_noise_inhib_scale

        self.abcdParams = AbcdParams(anatomy)

        self.synapse = Synapse(anatomy=anatomy, neurotransmitters=neurotransmitters, S_dist=S_dist, S_bias=S_bias,
                               S_scale=S_scale, S_prob=S_prob, delay_start=delay_start, delay_end=delay_end,
                               with_delay=with_delays, dt=dt)
        self.synapse.populate_S()

        self.v = -65 * np.ones(self.n_neurons)
        self.u = self.abcdParams.b * self.v

        self.threshold = np.ones(self.n_neurons) * 30
        self.spiked = np.array([]).astype('int')

        self.store_spikes = store_spikes
        if store_spikes:
            if preallocate_mem:
                self.spike_store = np.zeros((n_timesteps, self.n_neurons), dtype=np.bool_)
            else:
                self.spike_store = []

        self.store_v = store_v
        if store_v:
            self.v_store = []

        self.store_u = store_u
        if store_u:
            self.u_store = []

        self.random_batch_size = random_batch_size
        self.background_noise = BackgroundNoise(dt, device, n_timesteps, self.anatomy, batch_size=random_batch_size)

        self.i_e_noise = None
        self.i_i_noise = None

        self.i = None

        self.has_resting_membrane_potenital = None

        self.preallocate_mem = preallocate_mem

    def stable_resting_membrane_potential(self):
        self.has_resting_membrane_potenital = ((5 - self.abcdParams.b) ** 2 - 4 * self.abcdParams.a * 140) >= 0

    def epsp_dist(self):
        for i, n_neurons in enumerate(self.synapse.cumulative_n_neurons):
            if i == 0:
                s_by_anatomy = self.synapse.S[0:self.cumulative_n_neurons_anatomy[0], :].reshape(-1)
                g_check = s_by_anatomy[~(s_by_anatomy == 0)]
                b = 0.2
                v = - (5 - b) - ((5 - b) ** 2 - 4 * 0.04 * 140) ** (1 / 2)

    def update_g(self):

        # needs to for specific neurotransmitters

        for i, t_g in enumerate(self.synapse.neurotransmitters['Time constant g']):

            if i == 0:
                self.synapse.g[:, i] = (- self.synapse.g[:, i] / t_g \
                                        + np.sum(self.synapse.S[0:self.synapse.cumulative_n_neurons[i], :] \
                                                     [self.spiked[(0 <= self.spiked) & (
                                    self.spiked < self.synapse.cumulative_n_neurons[i])]
                                                 , :], axis=0)) * self.dt
            else:
                self.synapse.g[:, i] = (- self.synapse.g[:, i] / t_g \
                                        + np.sum(self.synapse.S[self.synapse.cumulative_n_neurons[i - 1]:
                                                                self.synapse.cumulative_n_neurons[i], :] \
                                                     [
                                                 self.spiked[(self.synapse.cumulative_n_neurons[i - 1] <= self.spiked) &
                                                             (self.spiked < self.synapse.cumulative_n_neurons[i])]
                                                 - self.synapse.cumulative_n_neurons[i - 1], :], axis=0)) * self.dt

    def update_g_delay(self):

        for i, t_g in enumerate(self.synapse.neurotransmitters['Time constant g']):

            self.synapse.g[:, i] += - self.synapse.g[:, i] / t_g + self.synapse.g_delay[:, 0, i]
            self.synapse.g_delay[:, :-1, i] = self.synapse.g_delay[:, 1:, i]
            self.synapse.g_delay[:, -1, i] = np.zeros(self.n_neurons)

            if i == 0:
                synaptic_current_summed = self.synapse.S_delay[0:self.synapse.cumulative_n_neurons[i], :, :] \
                    [self.spiked[(0 <= self.spiked) & (self.spiked < self.synapse.cumulative_n_neurons[i])], :, :]
                if synaptic_current_summed.ndim > 2:
                    self.synapse.g_delay[:, -self.synapse.S_delay.shape[2]:, i] \
                        += np.sum(synaptic_current_summed, axis=0) * self.dt
                else:
                    self.synapse.g_delay[:, -self.synapse.S_delay.shape[2]:, i] \
                        += synaptic_current_summed * self.dt

                # needs to be S not g_delay in sum
            else:
                synaptic_current_summed = self.synapse.S_delay[self.synapse.cumulative_n_neurons[i - 1]:
                                                               self.synapse.cumulative_n_neurons[i], :, :] \
                    [self.spiked[(self.synapse.cumulative_n_neurons[i - 1] <= self.spiked) &
                                 (self.spiked < self.synapse.cumulative_n_neurons[i])]
                     - self.synapse.cumulative_n_neurons[i - 1], :, :]
                if synaptic_current_summed.ndim > 2:
                    self.synapse.g_delay[:, -self.synapse.S_delay.shape[2]:, i] \
                        += np.sum(synaptic_current_summed, axis=0) * self.dt
                else:
                    self.synapse.g_delay[:, -self.synapse.S_delay.shape[2]:, i] \
                        += synaptic_current_summed * self.dt

                self.synapse.g_delay[:, -self.synapse.S_delay.shape[2]:, i] \
                    += np.sum(self.synapse.S_delay[self.synapse.cumulative_n_neurons[i - 1]:
                                                   self.synapse.cumulative_n_neurons[i], :, :] \
                                  [self.spiked[(self.synapse.cumulative_n_neurons[i - 1] <= self.spiked) &
                                               (self.spiked < self.synapse.cumulative_n_neurons[i])]
                                   - self.synapse.cumulative_n_neurons[i - 1], :, :], axis=0) * self.dt

    def update_I_synapes(self):

        for i, V in enumerate(self.synapse.nt_V):

            if i == 0:
                self.synapse.I_synapse = self.synapse.g[:, i] * (self.v - V)
            else:
                self.synapse.I_synapse += self.synapse.g[:, i] * (self.v - V)

    def update_i_noise(self):

        self.i_e_noise = self.background_noise.ge_noise * (self.v - self.background_noise.noise_V_e)
        self.i_i_noise = self.background_noise.gi_noise * (self.v - self.background_noise.noise_V_i)

    def sum_current(self):

        self.i = self.i_i_noise + self.i_e_noise + self.synapse.I_synapse

    def update_v(self):
        self.v += self.sub_dt * (0.04 * self.v ** 2 + 5 * self.v + 140 - self.u - self.i)

    def step(self):

        if self.store_spikes:
            if self.preallocate_mem:
                self.spike_store[self.n_steps, self.spiked] = True
            else:
                self.spike_store.append(self.spiked)
        if self.store_v:
            self.v_store.append(self.v.copy().reshape(-1, 1))
        if self.store_u:
            self.u_store.append(self.u.copy().reshape(-1, 1))

        self.update_I_synapes()

        if self.with_delay:
            self.update_g_delay()
        else:
            self.update_g()

        self.background_noise.update_g_noise()

        self.update_i_noise()

        self.sum_current()

        self.v[self.spiked] = self.abcdParams.c[self.spiked]
        self.u[self.spiked] += self.abcdParams.d[self.spiked]

        for _ in range(int(self.dt / self.sub_dt)):
            self.update_v()
            self.u += self.abcdParams.a * (self.v * self.abcdParams.b - self.u) * self.sub_dt

        self.n_steps += 1

        self.spiked = np.where(self.v > 30)[0]

        if any(self.v < -150) or any(self.v > 200):
            raise ValueError('Numerical instability detected, a neuron has an absoluted p.d. of {}' \
                             .format(str(np.max(np.abs(self.v)))))
