import torch
import torch.nn as nn
import torch.functional as f


class CNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.network = nn.Sequential(

            nn.Conv2d(1, 5, kernel_size=3, padding='same'),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(1, 4), stride=(1, 4)),

            nn.Conv2d(5, 5, kernel_size=3, padding='same'),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(1, 5), stride=(1, 5)),

            nn.Conv2d(5, 5, kernel_size=3, padding='same'),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(1, 5), stride=(1, 5)),

            nn.Conv2d(5, 5, kernel_size=3, padding='same'),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(1, 2)),

            nn.Conv2d(5, 5, kernel_size=3, padding='same'),
            nn.ReLU(),
            nn.AvgPool2d(kernel_size=(1, 5)),

            nn.Conv2d(5, 5, kernel_size=(5, 1)),
            nn.ReLU(),

            nn.Flatten(),
            nn.Linear(5 * 46, 20),
            nn.ReLU(),

            nn.Flatten(),
            nn.Linear(20, 2)

        )

        self.optimiser = torch.optim.Adam(self.network.parameters(),
                                          lr=0.001, betas=(0.9, 0.999))

    def forward(self, x):
        output = self.network(x)
        # output[0, 0] = nn.Sigmoid(output[0, 0])
        return output


def loss_function(output, target_classifier, target_regression):
    classifier_loss = torch.nn.BCELoss(output[:, 0], target_classifier)
    regression_loss = torch.nn.MSELoss(output[:, 1], target_regression)
    return (classifier_loss + regression_loss)/2


def optimiser(params):
    torch.optim.Adam(params, lr=0.001, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)
