import numpy as np
import time

n_ex_neurons = 800
n_inhib_neurons = 200

n_neurons = n_ex_neurons + n_inhib_neurons

# model params

a = 0.02 * np.ones(n_neurons)
b = 0.2* np.ones(n_neurons)
c = -65 * np.ones(n_neurons)
d = 2 * np.ones(n_neurons)

S = np.concatenate([0.5 * np.random.rand(n_neurons, n_ex_neurons), -np.random.rand(n_neurons, n_inhib_neurons)], axis=1) # synatptic weights

v = -65 * np.ones(n_neurons)
u = b * v

np.random.seed(123)

v = -65 * np.ones(n_neurons)
firing = []
queue_STDP = np.zeros((0, 2))

t0 = time.time()
for t in range(100000):  # were t increases in increments of 1 ms
    I = 5 * np.random.randn(n_neurons)
    fired_logic = v > 30
    fired = np.where(fired_logic)[0]
    fired_time = np.concatenate((fired.reshape(-1, 1), fired.reshape(-1, 1) * 0 + t), axis=1)
    firing.append(fired_time)
    # queue_STDP += 1
    # queue_STDP = STDP_queue_nonjit(queue_STDP, fired)

    v[fired] = c[fired]
    u[fired] = u[fired] + d[fired]
    I = I + np.sum(S[:, fired], axis=1)
    v += 0.5 * (0.04 * v ** 2 + 5 * v + 140 - u + I);  # step 0.5 ms
    v += 0.5 * (0.04 * v ** 2 + 5 * v + 140 - u + I);  # for numerical
    u += a * (b * v - u);

t1 = time.time()
print(t1-t0)