import torch
import numpy as np
import matplotlib.pyplot as plt
import datetime


def lognormal(x, mean, std):
    return np.exp(-((np.log(x) - mean)**2)/(2*std**2))/(std*x*(2*np.pi)**(1/2))


def replace_neg_conductance(inhib_S, S_inhib_std, S_inhib_mean):

    replace_inhib = inhib_S < 0
    inhib_S[replace_inhib] = torch.randn(torch.count_nonzero(replace_inhib)) * S_inhib_std + S_inhib_mean
    if any(inhib_S < 0):
        replace_neg_conductance(inhib_S, S_inhib_std, S_inhib_mean)

    return inhib_S


class MAT:
    def __init__(self, n_neurons, n_timesteps, timestep, Vl=-70, Ve = 0, Vi=-80, Tme=20, Tmi=10, prop_excit=0.8, Tse=1,
                 Tsi=2, T1=10, T2=200, We=-55, Wi=-57, a1e_mean=1.5, a1e_std=0.25, a2e=0.5, a1i=3, a2i=0,
                 syn_delay_range_excit=(3, 5), syn_delay_range_inhib=(2,4), S_excit_log_mean = -5.543,
                 S_excit_log_std=1.3, S_inhib_mean=0.0217,
                 S_inhib_std=0.00171, n_excit_inputs=100, n_inhib_inputs=50, ge0_bg=0.123, gi0_bg=0.322, Ee_bg=0,
                 Ei_bg=-80, sigma_e_bg=0.0163, sigma_i_bg=0.0265, Tse_bg=2.7, Tsi_bg = 10.5, A_ = 0.015,
                 n_neurons_per_freq=(100, 100, 100), frequency=(7, 10, 20)):

        self.ge0_bg = ge0_bg
        self.gi0_bg = gi0_bg
        self.Ee_bg = Ee_bg
        self.Ei_bg = Ei_bg
        self.sigma_e_bg = sigma_e_bg
        self.sigma_i_bg = sigma_i_bg
        self.Tse_bg = Tse_bg
        self.Tsi_bg = Tsi_bg
        self.A_ = A_
        self.n_neurons_per_freq = n_neurons_per_freq
        self.frequency = frequency

        self.t = 0
        self.step_n = 0
        self.n_neurons = n_neurons
        self.n_timesteps = n_timesteps
        self.dt = timestep
        self.n_excit = int(prop_excit*self.n_neurons)
        self.n_inhib = self.n_neurons - self.n_excit
        self.Vl = Vl  # mv
        self.Ve = Ve  # mv
        self.Vi = Vi  # mv
        self.Tm = torch.ones(self.n_neurons)*Tme
        self.Tm[self.n_excit:] = Tmi
        self.spike_time_store = torch.zeros([self.n_neurons, 1000])
        self.spike_time_store[:] = float('NaN')
        self.spike_times = []
        self.spiking = torch.zeros(self.n_neurons)

        self.syn_delay_range_excit = syn_delay_range_excit
        self.syn_delay_range_inhib = syn_delay_range_inhib
        self.delay_lower_step_excit = int(torch.round(torch.Tensor([self.syn_delay_range_excit[0] / self.dt])))
        self.delay_upper_step_excit = int(torch.round(torch.Tensor([self.syn_delay_range_excit[1] / self.dt])))
        self.delay_lower_step_inhib = int(torch.round(torch.Tensor([self.syn_delay_range_inhib[0] / self.dt])))
        self.delay_upper_step_inhib= int(torch.round(torch.Tensor([self.syn_delay_range_inhib[1] / self.dt])))
        self.g_increase_store_excit = torch.zeros((self.n_neurons, self.delay_upper_step_excit))
        self.g_increase_store_inhib = torch.zeros((self.n_neurons, self.delay_upper_step_inhib))
        self.ge = torch.zeros(self.n_neurons)
        self.gi = torch.zeros(self.n_neurons)
        self.ge_bg = torch.ones(self.n_neurons)*self.ge0_bg
        self.gi_bg = torch.ones(self.n_neurons)*self.gi0_bg

        self.Vm = 8*torch.rand(self.n_neurons)-66

        self.connected_excit = torch.zeros((self.n_excit, self.n_neurons))
        self.connected_inhib = torch.zeros((self.n_inhib, self.n_neurons))
        self.n_excit_inputs = n_excit_inputs
        self.n_inhib_inputs = n_inhib_inputs
        self.S_excit_log_mean = S_excit_log_mean
        self.S_excit_log_std = S_excit_log_std
        self.S_inhib_mean = S_inhib_mean
        self.S_inhib_std = S_inhib_std

        self.S_excit = torch.zeros((self.n_excit, self.n_neurons))
        self.S_excit_with_delays = torch.zeros((self.n_excit, self.n_neurons,
                                                        self.delay_upper_step_excit-self.delay_lower_step_excit))
        self.S_inhib = torch.zeros((self.n_inhib, self.n_neurons))
        self.S_inhib_with_delays = torch.zeros(((self.n_inhib, self.n_neurons,
                                                        self.delay_upper_step_inhib-self.delay_lower_step_inhib)))

        self.excit_S = torch.zeros(self.n_excit_inputs*self.n_neurons).log_normal_(self.S_excit_log_mean,
                                                                                   S_excit_log_std)
        self.inhib_S = torch.randn(self.n_inhib_inputs*self.n_neurons)*self.S_inhib_std + self.S_inhib_mean

        self.inhib_S = replace_neg_conductance(self.inhib_S, self.S_inhib_std, self.S_inhib_mean)

        self.Tse = Tse
        self.Tsi = Tsi

        # Threshold paramaters

        self.T1 = T1
        self.T2 = T2
        self.We = We
        self.Wi = Wi
        self.a1e_mean = a1e_mean
        self.a1e_std = a1e_std
        self.a1e = torch.randn(self.n_neurons)*self.a1e_std + self.a1e_mean
        self.a2e = a2e
        self.a1i = a1i
        self.a2i = a2i

        self.threshold = torch.ones(self.n_neurons)
        self.threshold[0:self.n_excit] = self.We
        self.threshold[self.n_excit:-1] = self.Wi

        for i in range(self.n_neurons):
            self.connected_excit[torch.randperm(self.n_excit)[0:self.n_excit_inputs], i] = 1
            self.connected_inhib[torch.randperm(self.n_inhib)[0:self.n_inhib_inputs], i] = 1

        self.connected_excit = self.connected_excit.bool()
        self.connected_inhib = self.connected_inhib.bool()

        self.S_excit[self.connected_excit] = self.excit_S
        self.S_excit_with_delays[self.connected_excit,
                                 torch.randint(int(self.delay_upper_step_excit-self.delay_lower_step_excit),
                                               (self.n_excit_inputs*self.n_neurons,))] = self.excit_S

        self.S_inhib[self.connected_inhib] = self.inhib_S
        self.S_inhib_with_delays[self.connected_inhib,
                                 torch.randint(
                                     int(self.delay_upper_step_inhib - self.delay_lower_step_inhib),
                                     (self.n_inhib_inputs*self.n_neurons,))] = self.inhib_S

        self.background_noise = BackgroundNoise(self.dt, self.n_timesteps, self.n_neurons,
                                                n_neurons_per_freq=self.n_neurons_per_freq, frequency=self.frequency,
                                                sigma_e=self.sigma_e_bg, sigma_i=self.sigma_i_bg, t_se=self.Tse_bg,
                                                t_si=self.Tsi_bg, A_=self.A_)

    def H_function(self, neuron_type='excit'):

        if neuron_type == 'excit':
            return torch.nansum(self.a1e*torch.exp(-self.spike_time_store[:self.n_excit, :]/self.T1) + \
                                self.a2e*torch.exp(-self.spike_time_store[:self.n_excit, :]/self.T2), dim=1)
        if neuron_type == 'inhib':
            return torch.nansum(self.a1i*torch.exp(-self.spike_time_store[self.n_excit:, :]/self.T1) + \
                   self.a2i*torch.exp(-self.spike_time_store[self.n_excit:, :]/self.T2), dim=1)
        # quicker to take a out of ∑?

    def update_treshold(self):

        self.threshold[0:self.n_excit] = self.H_function(neuron_type='excit') + self.We
        self.threshold[self.n_excit:] = self.H_function(neuron_type='inhib') + self.Wi

    def step(self):

        self.ge_bg += (-(self.ge_bg - self.ge0_bg)/self.Tse_bg + self.background_noise.Ee_noise[self.step_n, :])*self.dt
        self.gi_bg += (-(self.gi_bg - self.gi0_bg)/self.Tsi_bg + self.background_noise.Ei_noise[self.step_n, :])*self.dt

        self.Vm += (-(self.Vm - self.Vl) - self.Tm*(self.ge*(self.Vm-self.Ve)+self.gi*(self.Vm-self.Vi)) -
                    self.Tm * (self.ge_bg * (self.Vm - self.Ee_bg) + self.gi_bg * (self.Vm - self.Ei_bg))) * \
                   (self.dt/self.Tm)

        self.ge += (-self.ge/self.Tse + self.g_increase_store_excit[:, 0])*self.dt
        self.g_increase_store_excit[:, :(self.delay_upper_step_excit-1)] = self.g_increase_store_excit[:, 1:]
        self.g_increase_store_excit[:, -1] = 0

        self.gi += (-self.gi/self.Tsi + self.g_increase_store_inhib[:, 0])*self.dt
        self.g_increase_store_inhib[:, :(self.delay_upper_step_inhib - 1)] = self.g_increase_store_inhib[:, 1:]
        self.g_increase_store_inhib[:, -1] = 0

        self.spiking = self.Vm > self.threshold

        self.g_increase_store_excit[:, self.delay_lower_step_excit:] += \
            torch.einsum('n, nmp -> mp',
                         self.spiking[:self.n_excit].float(),
                         self.S_excit_with_delays)

        self.g_increase_store_inhib[:, self.delay_lower_step_inhib:] += \
            torch.einsum('n, nmp -> mp',
                         self.spiking[self.n_excit:].float(),
                         self.S_inhib_with_delays)

        self.t += self.dt
        self.step_n += 1

        # update spike time store for H function

        self.spike_time_store[self.spiking, 1:] = self.spike_time_store[self.spiking,
                                                                        :(self.spike_time_store.shape[0]-1)]
        self.spike_time_store += self.dt
        self.spike_time_store[self.spiking, 0] = torch.zeros(torch.count_nonzero(self.spiking))

        self.update_treshold()

        # add spike time store for all spikes

        self.spike_times.append(self.spiking)


class BackgroundNoise:

    def __init__(self, timestep, n_timesteps, n_neurons, n_neurons_per_freq=(100, 100, 100), frequency=(7, 10, 20),
                 sigma_e=0.0163, sigma_i=0.0265, t_se=2.7, t_si=10.5, A_=0.015):

        assert len(n_neurons_per_freq) == len(frequency), 'Length of n_neurons_per_freq must be the same as frequency'

        self.Ei_noise = torch.randn(n_timesteps, n_neurons) * ((2*sigma_i**2)/t_si) ** (1/2)

        phase_prog = torch.linspace(0, timestep * 2 * torch.pi * (n_timesteps - 1) / 1000, n_timesteps)

        sin_store = torch.zeros((n_timesteps, n_neurons))

        n_total = 0
        rand_perm_units = torch.randperm(n_neurons)
        for i, n_units in enumerate(n_neurons_per_freq):
            phase = phase_prog.repeat(n_units, 1).T*frequency[i]
            phase = phase + 2 * torch.pi * torch.rand(n_units)
            A = A_/2 + A_ * torch.pi * torch.rand(n_units)
            sin_store[:, rand_perm_units[n_total:(n_total + n_units)]] = A * torch.sin(phase) * \
                                                                         torch.randn(phase.size())
            n_total += n_units

        self.Ee_noise = torch.randn(n_timesteps, n_neurons) * ((2*sigma_e**2)/t_se) ** (1/2) + sin_store

'''
        a = torch.zeros((2, 3, 4))
        b = torch.zeros((1,2))
        a[[1, 1, 0, 0], [0, 1, 2, 1], [2, 1, 1, 0]] = 1

        c = torch.einsum('n, nmp -> np', b, a)

'''
